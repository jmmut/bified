/**
 * @file parser.test.cpp
 */

#include "catch2/catch.hpp"
#include "Reader.h"

TEST_CASE( "Read square without offsets", "[read_square]" ) {
    std::string filename;

    filename = "resources/a.txt";
    SECTION("Read square for " + filename) {
        std::ifstream file{filename};
        if (not file) {
            throw std::logic_error{"can not find resource " + filename + " for testing"};
        }
        DetectedNewLines newLines;
        auto rows = 3;
        auto columns = 4;
        auto textSquare = readSquareSkipping(file, rows, columns, 0, 0, newLines);

        SECTION("count text square sizes") {
            REQUIRE(textSquare.size() == rows);
            for (const auto &row : textSquare) {
                REQUIRE(row.size() == columns);
            }
        }
        SECTION("check text square content") {
            REQUIRE(textSquare[0][0] == 'a');
            REQUIRE(textSquare[0][1] == 0);
            REQUIRE(textSquare[0][2] == 0);
            REQUIRE(textSquare[0][3] == 0);
            REQUIRE(textSquare[1][0] == 0);
            REQUIRE(textSquare[2][0] == 0);
        }
    }

    SECTION("check newlines") {
        for (std::string filename : {"resources/ab_newline_linux.txt", "resources/ab_newline_mac.txt",
                "resources/ab_newline_windows.txt"}) {
            SECTION("Read square for " + filename) {
                std::ifstream file{filename};
                if (not file) {
                    throw std::logic_error{"can not find resource " + filename + " for testing"};
                }
                DetectedNewLines newLines;
                auto rows = 3;
                auto columns = 4;
                auto textSquare = readSquareSkipping(file, rows, columns, 0, 0, newLines);

                SECTION("count text square sizes") {
                    REQUIRE(textSquare.size() == rows);
                    for (const auto &row : textSquare) {
                        REQUIRE(row.size() == columns);
                    }
                }
                SECTION("check text square content") {
                    REQUIRE(textSquare[0][0] == 'a');
                    REQUIRE(textSquare[0][1] == 0);
                    REQUIRE(textSquare[0][2] == 0);
                    REQUIRE(textSquare[0][3] == 0);
                    REQUIRE(textSquare[1][0] == 'b');
                    REQUIRE(textSquare[2][0] == 0);
                }
            }
        }
    }

    filename = "resources/long_lines.txt";
    SECTION("Read square for " + filename) {
        std::ifstream file{filename};
        if (not file) {
            throw std::logic_error{"can not find resource " + filename + " for testing"};
        }
        DetectedNewLines newLines;
        auto rows = 3;
        auto columns = 4;
        auto textSquare = readSquareSkipping(file, rows, columns, 0, 0, newLines);

        SECTION("count text square sizes") {
            REQUIRE(textSquare.size() == rows);
            for (const auto &row : textSquare) {
                REQUIRE(row.size() == columns);
            }
        }
        SECTION("check text square content") {
            REQUIRE(textSquare[0][0] == 'a');
            REQUIRE(textSquare[0][1] == 0);
            REQUIRE(textSquare[0][2] == 0);
            REQUIRE(textSquare[1][0] == 'b');
            REQUIRE(textSquare[1][1] == 'b');
            REQUIRE(textSquare[1][2] == 0);
            REQUIRE(textSquare[1][3] == 0);
            REQUIRE(textSquare[2][0] == 'c');
            REQUIRE(textSquare[2][1] == 'c');
            REQUIRE(textSquare[2][2] == 'c');
            REQUIRE(textSquare[2][3] == 0);
        }
    }
}

TEST_CASE( "Read square with row offset", "[read_square]" ) {
    std::string filename;

    filename = "resources/a.txt";
    SECTION("Read square for " + filename) {
        std::ifstream file{filename};
        if (not file) {
            throw std::logic_error{"can not find resource " + filename + " for testing"};
        }
        DetectedNewLines newLines;
        auto rows = 3;
        auto columns = 4;
        auto textSquare = readSquareSkipping(file, rows, columns, 1, 2, newLines);

        SECTION("count text square sizes") {
            REQUIRE(textSquare.size() == rows);
            for (const auto &row : textSquare) {
                REQUIRE(row.size() == columns);
            }
        }
        SECTION("check text square content") {
            REQUIRE(textSquare[0][0] == 0);
            REQUIRE(textSquare[0][1] == 0);
            REQUIRE(textSquare[0][2] == 0);
            REQUIRE(textSquare[0][3] == 0);
            REQUIRE(textSquare[1][0] == 0);
            REQUIRE(textSquare[2][0] == 0);
        }
    }

    SECTION("check newlines") {
        for (std::string filename : {"resources/ab_newline_linux.txt", "resources/ab_newline_mac.txt",
                "resources/ab_newline_windows.txt"}) {
            SECTION("Read square for " + filename) {
                std::ifstream file{filename};
                if (not file) {
                    throw std::logic_error{"can not find resource " + filename + " for testing"};
                }
                DetectedNewLines newLines;
                auto rows = 3;
                auto columns = 4;
                auto textSquare = readSquareSkipping(file, rows, columns, 1, 0, newLines);

                SECTION("count text square sizes") {
                    REQUIRE(textSquare.size() == rows);
                    for (const auto &row : textSquare) {
                        REQUIRE(row.size() == columns);
                    }
                }
                SECTION("check text square content") {
                    REQUIRE(textSquare[0][0] == 'b');
                    REQUIRE(textSquare[0][1] == 0);
                    REQUIRE(textSquare[0][2] == 0);
                    REQUIRE(textSquare[0][3] == 0);
                    REQUIRE(textSquare[1][0] == 0);
                    REQUIRE(textSquare[2][0] == 0);
                }
            }
        }
    }

    filename = "resources/long_lines.txt";
    SECTION("Read square for " + filename) {
        std::ifstream file{filename};
        if (not file) {
            throw std::logic_error{"can not find resource " + filename + " for testing"};
        }
        DetectedNewLines newLines;
        auto rows = 3;
        auto columns = 4;
        auto textSquare = readSquareSkipping(file, rows, columns, 1, 0, newLines);

        SECTION("count text square sizes") {
            REQUIRE(textSquare.size() == rows);
            for (const auto &row : textSquare) {
                REQUIRE(row.size() == columns);
            }
        }
        SECTION("check text square content") {
            REQUIRE(textSquare[0][0] == 'b');
            REQUIRE(textSquare[0][1] == 'b');
            REQUIRE(textSquare[0][2] == 0);
            REQUIRE(textSquare[0][3] == 0);
            REQUIRE(textSquare[1][0] == 'c');
            REQUIRE(textSquare[1][1] == 'c');
            REQUIRE(textSquare[1][2] == 'c');
            REQUIRE(textSquare[1][3] == 0);
        }
    }

    filename = "resources/long_lines.txt";
    SECTION("Read square after EOF for " + filename) {
        std::ifstream file{filename};
        if (not file) {
            throw std::logic_error{"can not find resource " + filename + " for testing"};
        }
        DetectedNewLines newLines;
        auto rows = 3;
        auto columns = 4;
        auto textSquare = readSquareSkipping(file, rows, columns, 35, 0, newLines);

        SECTION("count text square sizes") {
            REQUIRE(textSquare.size() == rows);
            for (const auto &row : textSquare) {
                REQUIRE(row.size() == columns);
            }
        }
        SECTION("check text square content") {
            REQUIRE(textSquare[0][0] == '\0');
        }
    }
}

