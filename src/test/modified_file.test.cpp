/**
 * @file parser.test.cpp
 */

#include <sstream>
#include <iostream>
#include <map>

/**
 * These stream overloads (or at least their prototypes) have to appear before the include<catch>
 */
std::ostream& operator<<(std::ostream &out, const std::pair<long, long> &pair) {
    out << ("{" + std::to_string(pair.first) + ", " + std::to_string(pair.second) + "}");
    return out;
}

bool operator==(const std::pair<long, long> &firstPair, const std::pair<long, long> &secondPair) {
    return firstPair.first == secondPair.first and firstPair.second == secondPair.second;
}

std::ostream& operator<<(std::ostream &out, const std::map<long, long> &map) {
    if (map.empty()) {
        out << "{}";
    } else {
        auto iterator = map.begin();
        out << iterator.operator*();
        while (++iterator != map.end()) {
            out << ", " << iterator.operator*();
        }
    }
    return out;
}

#include "catch2/catch.hpp"
#include "ModifiedFile.h"
#include "Reader.h"


TEST_CASE( "ModifiedFile with a.txt", "[modified_file]" ) {

    ModifiedFile modifiedFile{"resources/a.txt"};

    modifiedFile.changeLetterAt(0, 'z');

    std::stringstream ss;
    modifiedFile.copyTo(ss);

    REQUIRE(ss.str() == "z");
}


void requireChangeLetter(ModifiedFile &modifiedFile, int positionInNewFile, char newLetter, const char *result) {
    modifiedFile.changeLetterAt(positionInNewFile, newLetter);

    std::stringstream ss;
    modifiedFile.copyTo(ss);

    REQUIRE(ss.str() == result);
}

TEST_CASE( "ModifiedFile change letter with ab_newline_linux.txt", "[modified_file]" ) {

    ModifiedFile modifiedFile{"resources/ab_newline_linux.txt"};

    SECTION("change first letter") {
        requireChangeLetter(modifiedFile, 0, 'z', "z\nb\n");
    }

    SECTION("change newline") {
        requireChangeLetter(modifiedFile, 1, 'z', "azb\n");
    }

    SECTION("change second letter") {
        requireChangeLetter(modifiedFile, 2, 'z', "a\nz\n");
    }
    
    SECTION("change last letter") {
        requireChangeLetter(modifiedFile, 3, 'z', "a\nbz");
    }
}

TEST_CASE( "ModifiedFile change letter with ab_newline_mac.txt", "[modified_file]" ) {

    ModifiedFile modifiedFile{"resources/ab_newline_mac.txt"};

    SECTION("change first letter") {
        requireChangeLetter(modifiedFile, 0, 'z', "z\rb\r");
    }

    SECTION("change newline") {
        requireChangeLetter(modifiedFile, 1, 'z', "azb\r");
    }

    SECTION("change second letter") {
        requireChangeLetter(modifiedFile, 2, 'z', "a\rz\r");
    }

    SECTION("change last letter") {
        requireChangeLetter(modifiedFile, 3, 'z', "a\rbz");
    }
}

TEST_CASE( "ModifiedFile change letter with ab_newline_windows.txt", "[modified_file]" ) {

    ModifiedFile modifiedFile{"resources/ab_newline_windows.txt"};

    SECTION("change first letter") {
        requireChangeLetter(modifiedFile, 0, 'z', "z\r\nb\r\n");
    }

    SECTION("change newline 1") {
        requireChangeLetter(modifiedFile, 1, 'z', "az\nb\r\n");
    }

    SECTION("change newline 2") {
        requireChangeLetter(modifiedFile, 2, 'z', "a\rzb\r\n");
    }

    SECTION("change second letter") {
        requireChangeLetter(modifiedFile, 3, 'z', "a\r\nz\r\n");
    }

    SECTION("change last letter") {
        requireChangeLetter(modifiedFile, 5, 'z', "a\r\nb\rz");
    }
}


void requireDeleteLetter(ModifiedFile &modifiedFile, int positionInNewFile, const char *result) {
    modifiedFile.deleteLetterAt(positionInNewFile);

    std::stringstream ss;
    modifiedFile.copyTo(ss);

    REQUIRE(ss.str() == result);
}

TEST_CASE( "ModifiedFile delete letter with ab_newline_linux.txt", "[modified_file]" ) {

    ModifiedFile modifiedFile{"resources/ab_newline_linux.txt"};

    SECTION("delete first letter") {
        requireDeleteLetter(modifiedFile, 0, "\nb\n");
    }

    SECTION("delete newline") {
        requireDeleteLetter(modifiedFile, 1, "ab\n");
    }

    SECTION("delete second letter") {
        requireDeleteLetter(modifiedFile, 2, "a\n\n");
    }

    SECTION("delete last letter") {
        requireDeleteLetter(modifiedFile, 3, "a\nb");
    }
}

TEST_CASE( "ModifiedFile delete letter with ab_newline_mac.txt", "[modified_file]" ) {

    ModifiedFile modifiedFile{"resources/ab_newline_mac.txt"};

    SECTION("delete first letter") {
        requireDeleteLetter(modifiedFile, 0, "\rb\r");
    }

    SECTION("delete newline") {
        requireDeleteLetter(modifiedFile, 1, "ab\r");
    }

    SECTION("delete second letter") {
        requireDeleteLetter(modifiedFile, 2, "a\r\r");
    }

    SECTION("delete last letter") {
        requireDeleteLetter(modifiedFile, 3, "a\rb");
    }
}

TEST_CASE( "ModifiedFile delete letter with ab_newline_windows.txt", "[modified_file]" ) {

    ModifiedFile modifiedFile{"resources/ab_newline_windows.txt"};

    SECTION("delete first letter") {
        requireDeleteLetter(modifiedFile, 0, "\r\nb\r\n");
    }

    SECTION("delete newline 1") {
        requireDeleteLetter(modifiedFile, 1, "a\nb\r\n");
    }

    SECTION("delete newline 2") {
        requireDeleteLetter(modifiedFile, 2, "a\rb\r\n");
    }

    SECTION("delete second letter") {
        requireDeleteLetter(modifiedFile, 3, "a\r\n\r\n");
    }

    SECTION("delete last letter") {
        requireDeleteLetter(modifiedFile, 5, "a\r\nb\r");
    }
}

void requireInsertLetter(ModifiedFile &modifiedFile, int positionInNewFile, char newLetter, const char *result) {
    modifiedFile.insertLetterBefore(positionInNewFile, newLetter);

    std::stringstream ss;
    modifiedFile.copyTo(ss);

    REQUIRE(ss.str() == result);
}

TEST_CASE( "ModifiedFile insert letter with ab_newline_linux.txt", "[modified_file]" ) {

    ModifiedFile modifiedFile{"resources/ab_newline_linux.txt"};

    SECTION("insert first letter") {
        requireInsertLetter(modifiedFile, 0, 'z', "za\nb\n");
    }

    SECTION("insert newline") {
        requireInsertLetter(modifiedFile, 1, 'z', "az\nb\n");
    }

    SECTION("insert second letter") {
        requireInsertLetter(modifiedFile, 2, 'z', "a\nzb\n");
    }

    SECTION("insert last letter") {
        requireInsertLetter(modifiedFile, 3, 'z', "a\nbz\n");
    }

    SECTION("insert after last letter") {
        requireInsertLetter(modifiedFile, 4, 'z', "a\nb\nz");
    }
}

TEST_CASE( "ModifiedFile insert letter with ab_newline_mac.txt", "[modified_file]" ) {

    ModifiedFile modifiedFile{"resources/ab_newline_mac.txt"};

    SECTION("insert first letter") {
        requireInsertLetter(modifiedFile, 0, 'z', "za\rb\r");
    }

    SECTION("insert newline") {
        requireInsertLetter(modifiedFile, 1, 'z', "az\rb\r");
    }

    SECTION("insert second letter") {
        requireInsertLetter(modifiedFile, 2, 'z', "a\rzb\r");
    }

    SECTION("insert last letter") {
        requireInsertLetter(modifiedFile, 3, 'z', "a\rbz\r");
    }

    SECTION("insert after last letter") {
        requireInsertLetter(modifiedFile, 4, 'z', "a\rb\rz");
    }
}

TEST_CASE( "ModifiedFile insert letter with ab_newline_windows.txt", "[modified_file]" ) {

    ModifiedFile modifiedFile{"resources/ab_newline_windows.txt"};

    SECTION("insert first letter") {
        requireInsertLetter(modifiedFile, 0, 'z', "za\r\nb\r\n");
    }

    SECTION("insert newline 1") {
        requireInsertLetter(modifiedFile, 1, 'z', "az\r\nb\r\n");
    }

    SECTION("insert newline 2") {
        requireInsertLetter(modifiedFile, 2, 'z', "a\rz\nb\r\n");
    }

    SECTION("insert second letter") {
        requireInsertLetter(modifiedFile, 3, 'z', "a\r\nzb\r\n");
    }

    SECTION("insert last letter") {
        requireInsertLetter(modifiedFile, 5, 'z', "a\r\nb\rz\n");
    }

    SECTION("insert last letter") {
        requireInsertLetter(modifiedFile, 6, 'z', "a\r\nb\r\nz");
    }
}

TEST_CASE( "ModifiedFile movement over original file", "[modified_file]" ) {
    SECTION("move down 1 line") {
        DetectedNewLines newLines;
        ModifiedFile modifiedFile{"resources/ab_newline_linux.txt"};
        std::ifstream file{"resources/ab_newline_linux.txt"};
        file.clear();
        file.seekg(0);
        char unusedLetter = file.get();
        auto l = consumeLinesUntilCount(file, 1, unusedLetter, newLines);
        consumeLettersUntilCountOrNewLine(file, 0, l);
        modifiedFile.insertLetterBefore(file.tellg() - 1L, 'z');
        std::stringstream ss;
        modifiedFile.copyTo(ss);

        REQUIRE(ss.str() == "a\nzb\n");
    }

    SECTION("move down 2 lines") {
        DetectedNewLines newLines;
        ModifiedFile modifiedFile{"resources/long_lines.txt"};
        std::ifstream file{"resources/long_lines.txt"};
        file.clear();
        file.seekg(0);
        char unusedLetter = file.get();
        auto l = consumeLinesUntilCount(file, 2, unusedLetter, newLines);
        consumeLettersUntilCountOrNewLine(file, 0, l);
        modifiedFile.insertLetterBefore(file.tellg() - 1L, 'z');
        std::stringstream ss;
        modifiedFile.copyTo(ss);

        REQUIRE(ss.str().substr(0, 10) == "a\nbb\nzccc\n");
    }

    SECTION("move down 2 lines, right 1 column") {
        DetectedNewLines newLines;
        ModifiedFile modifiedFile{"resources/long_lines.txt"};
        std::ifstream file{"resources/long_lines.txt"};
        file.clear();
        file.seekg(0);
        char unusedLetter = file.get();        
        auto l = consumeLinesUntilCount(file, 2, unusedLetter, newLines);
        consumeLettersUntilCountOrNewLine(file, 1, l);
        modifiedFile.insertLetterBefore(file.tellg() - 1L, 'z');
        std::stringstream ss;
        modifiedFile.copyTo(ss);

        REQUIRE(ss.str().substr(0, 10) == "a\nbb\nczcc\n");
    }
}

TEST_CASE( "ModifiedFile movement over virtual file", "[modified_file]" ) {
    SECTION("move down 1 line") {
        DetectedNewLines newLines;
        ModifiedFile file{"resources/ab_newline_linux.txt"};
        file.seekg(0);
        char unusedLetter = file.get();
        auto l = consumeLinesUntilCount(file, 1, unusedLetter, newLines);
        consumeLettersUntilCountOrNewLine(file, 0, l);
        file.insertLetterBefore(file.tellg() - 1L, 'z');
        std::stringstream ss;
        file.copyTo(ss);

        REQUIRE(ss.str() == "a\nzb\n");
    }

    SECTION("move down after EOF") {
        DetectedNewLines newLines;
        ModifiedFile file{"resources/ab_newline_linux.txt"};
        auto text = readSquareSkipping(file, 3, 3, 2, 0, newLines);
        REQUIRE(text[0][0] == '\0');
    }
    SECTION("move using seekg") {
        ModifiedFile file{"resources/ab_newline_linux.txt"};
        file.seekg(1);
        auto letter = file.get();
        REQUIRE(letter == '\n');
        REQUIRE(file.tellg() == 2);
        file.seekg(2);
        letter = file.get();
        REQUIRE(letter == 'b');
        REQUIRE(file.tellg() == 3);
        file.seekg(3);
        letter = file.get();
        REQUIRE(letter == '\n');
        REQUIRE(file.tellg() == 4);
    }

    SECTION("move using seekCoordinate") {
        ModifiedFile file{"resources/ab_newline_linux.txt"};
        auto letter = file.seekCoordinateAndGet(0, 1);
        REQUIRE(letter == '\n');
        REQUIRE(file.tellg() == 2);

        letter = file.seekCoordinateAndGet(1, 0);
        REQUIRE(letter == 'b');
        REQUIRE(file.tellg() == 3);

        letter = file.seekCoordinateAndGet(1, 1);
        REQUIRE(letter == '\n');
        REQUIRE(file.tellg() == 4);

        letter = file.seekCoordinateAndGet(0, 0);
        REQUIRE(letter == 'a');
        REQUIRE(file.tellg() == 1);
    }

    SECTION("move using cache") {
        ModifiedFile file{"resources/counter.txt", 10};

        auto letter = file.seekCoordinateAndGet(14, 0);
        CHECK(letter == '1');
        letter = file.get();
        CHECK(letter == '5');

        {
            const auto &lineToOffset = file.getLineToOffsetCache();
            REQUIRE(lineToOffset == std::map<long,long>{{14,33},{4,8}});
        }

        letter = file.seekCoordinateAndGet(13, 0);
        CHECK(letter == '1');
        letter = file.get();
        CHECK(letter == '4');

        {
            const auto &lineToOffset = file.getLineToOffsetCache();
            REQUIRE(lineToOffset == std::map<long, long>{{14, 33}, {4, 8}, {13, 30}});
        }

        letter = file.seekCoordinateAndGet(16, 0);
        CHECK(letter == '1');
        letter = file.get();
        CHECK(letter == '7');


        {
            const auto &lineToOffset = file.getLineToOffsetCache();
            REQUIRE(lineToOffset == std::map<long, long>{{14, 33}, {4, 8}, {13, 30}, {16, 39}});
        }
    }
}

TEST_CASE( "ModifiedFile delete and read", "[modified_file]" ) {
    SECTION("no movement") {
        DetectedNewLines newLines;
        ModifiedFile file{"resources/ab_newline_linux.txt"};
        file.seekg(0);
        file.get();
        auto position = file.tellg() - 1L;
        REQUIRE(position == 0);
        file.insertLetterBefore(position, 'z');

        std::stringstream ss;
        file.copyTo(ss);

        REQUIRE(ss.str() == "za\nb\n");

        file.seekg(0);
        auto text = readSquareSkipping(file, 2, 3, 0, 0, newLines);
        REQUIRE(text[0].size() == 3);
        REQUIRE(text[0][0] == 'z');
        REQUIRE(text[0][1] == 'a');
        REQUIRE(text[0][2] == '\0');
        REQUIRE(text[1][0] == 'b');
        REQUIRE(text[1][1] == '\0');
    }
}


TEST_CASE( "ModifiedFile search", "[modified_file]" ) {
    SECTION("single char at the beginning") {
        ModifiedFile file{"resources/ab_newline_linux.txt"};
        auto line = file.seekText("a", 0);
        REQUIRE(line == std::pair<long, long>{0, 0});
    }

    SECTION("single char at the middle") {
        ModifiedFile file{"resources/ab_newline_linux.txt"};
        auto line = file.seekText("b", 0);
        REQUIRE(line == std::pair<long, long>{1, 0});
    }

    SECTION("single char at the middle, starting at the beginning of the file") {
        ModifiedFile file{"resources/long_lines.txt"};
        auto line = file.seekText("AA", 0);
        REQUIRE(line == std::pair<long, long>{5, 54});
    }

    SECTION("single char at the middle, starting at the middle of the file") {
        ModifiedFile file{"resources/long_lines.txt"};
        file.seekCoordinateAndGet(2, 1);
        auto line = file.seekText("AA", 0);
        REQUIRE(line == std::pair<long, long>{5, 54});
    }
}
