// http://www.tldp.org/HOWTO/NCURSES-Programming-HOWTO/helloworld.html
// gcc <this_file> -o ncurses_test -lncurses

#include <ncurses.h>
#include <iostream>
#include <fstream>
#include <vector>
#include <tuple>
#include "Reader.h"
#include "ModifiedFile.h"

static const int CTRL_B = 2;
static const int CTRL_D = 4;
static const int CTRL_F = 6;
static const int CTRL_G = 7;
static const int CTRL_H = 263;
static const int CTRL_I = 9;
static const int CTRL_L = 12;
static const int CTRL_P = 16;
static const int CTRL_Q = 17;
static const int CTRL_R = 18;
static const int CTRL_S = 19;
static const int CTRL_U = 21;
static const int CTRL_V = 22;

static const int BACKSPACE_IN_LOCAL_TERMINAL = 127;
static const int ENTER_IN_LOCAL_TERMINAL = 10;

void modifyOffsets(int pressed, long &rows, long &columns, long rowsToMove, long columnsToMove);

bool isArrow(int pressed);

void printTextInScreen(int columns, const std::vector<std::string> &textInScreen, std::pair<long, long> startHighlight,
        std::pair<long, long> endHighlight);

void printHelp();
void printInfo(int rows, int columns, const DetectedNewLines &newLines, const std::string &filename);

template<typename T>
std::string concatenate(std::string delimiter, T elements);

bool isPrintable(int pressed);

void mainLoop(const std::string &filename);

std::string getNewLine(DetectedNewLines lines);

/**
 * Use RAII to call endwin() on exiting scope
 */
class NcursesInitializer{
public:
    NcursesInitializer() {
        initscr();
        noecho();
        keypad(stdscr, TRUE);   // enable use of F1, F2, etc, arrows, keypad.
        start_color();
    }

    virtual ~NcursesInitializer() {
        endwin();      /* End curses mode      */
    }
};

int main (int argc, char **argv) {
    try {
        if (argc != 2) {
            throw std::invalid_argument(addStackTrace("need 1 parameter: the file to edit"));
        }
        std::string filename{argv[1]};

        NcursesInitializer ncursesInitializer;

        mainLoop(filename);
    } catch (std::exception &e) {
        std::cout << "Aborting program due to exception: " << e.what() << std::endl;
        return 1;
    }

    return 0;
}

void mainLoop(const std::string &filename) {
    DetectedNewLines newLines;
    newLines.linux = false;
    newLines.mac = false;
    newLines.windows = false;
    int rows = 0, columns = 0;
    long skipFirstRows = 0;
    long skipFirstColumns = 0;
    int pressed = KEY_UP;

    ModifiedFile file{filename};

    bool shouldExit = false;
    while (not shouldExit) {
        long startHighlightColumn = -1;
        long endHighlightColumn = -1;
        if (isArrow(pressed)) {
            modifyOffsets(pressed, skipFirstRows, skipFirstColumns, 1, 1);
        }
        if (pressed == CTRL_B) {
            printHelp();
        }

        if (pressed == CTRL_V) {
            printInfo(rows, columns, newLines, filename);
        }
        if (pressed == CTRL_L) {
            clear();
            move(0, 0);
            printw("%s", file.getLog().c_str());
            printw("\n\npress any key to exit this screen");
            refresh();      /* Print it on to the real screen */
            getch();
        }
        if (pressed == KEY_DC) {
            file.seekCoordinateAndGet(skipFirstRows, skipFirstColumns);
            file.deleteLetterAt(file.tellg() - 1L);
        }

        if (pressed == KEY_BACKSPACE or pressed == CTRL_H or pressed == BACKSPACE_IN_LOCAL_TERMINAL) {
            file.seekCoordinateAndGet(skipFirstRows, skipFirstColumns);
            if (file.tellg() >= 2) {
                file.deleteLetterAt(file.tellg() - 2L);
                if (newLines.windows) {
                    throw std::logic_error(addStackTrace("backspace on windows newlines is not implemented"));
                }
            }
            if (skipFirstColumns != 0) {
                skipFirstColumns--;
            } else {
                if (skipFirstRows != 0) {
                    skipFirstRows--;
                }
            }
        }
        if (pressed == KEY_ENTER or pressed == ENTER_IN_LOCAL_TERMINAL) {
            file.seekCoordinateAndGet(skipFirstRows, skipFirstColumns);
            auto newLine = getNewLine(newLines);
            for (const auto &letter : newLine) {
                file.insertLetterBefore(file.tellg() - 1L, letter);
            }
            skipFirstColumns = 0;
            skipFirstRows++;
        }
        if (isPrintable(pressed)) {
            file.seekCoordinateAndGet(skipFirstRows, skipFirstColumns);
            file.insertLetterBefore(file.tellg() - 1L, pressed);
            ++skipFirstColumns;
        }
        if (pressed == CTRL_G) {
            move(rows - 1, 0);
            printw("enter line number to go: ");
            echo();
            char str[80];
            getstr(str);
            noecho();
            long parsedLine = std::strtol(str, nullptr, 10);
            skipFirstRows = parsedLine - 1;
        }
        if (pressed == CTRL_F) {
            move(rows - 1, 0);
            init_pair(2, COLOR_BLACK, COLOR_CYAN);
            attron(COLOR_PAIR(2));
            printw("enter text to search: ");
            attroff(COLOR_PAIR(2));
            echo();
            char str[80];
            getstr(str);
            std::string text{str};
            noecho();
            long lineNumber, columnNumber;
            std::tie(lineNumber, columnNumber) = file.seekText(text, skipFirstRows);
            if (lineNumber == -1) {
                move(rows - 1, 0);
                init_pair(3, COLOR_WHITE, COLOR_RED);
                attron(COLOR_PAIR(3));
                printw("text not found. Press any key");
                attroff(COLOR_PAIR(3));
                pressed = getch();      /* Wait for user input */
            } else {
                skipFirstRows = lineNumber;
                startHighlightColumn = columnNumber;
                endHighlightColumn = columnNumber + text.size();
            }
        }

        if (pressed == CTRL_S) {
            file.copyTo(filename + ".modified");
        }


        getmaxyx(stdscr, rows, columns);
        rows--; // to have space for status bar
        auto letter = file.seekCoordinateAndGet(skipFirstRows, skipFirstColumns);
        auto textInScreen = readSquare(file, rows, columns, skipFirstColumns, letter, newLines);
        printTextInScreen(columns, textInScreen, {0, startHighlightColumn}, {0, endHighlightColumn});


        move(rows, 0);
        init_pair(2, COLOR_BLACK, COLOR_CYAN);
        attron(COLOR_PAIR(2));
        printw("press CTRL-q to exit without saving. CTRL-b for help. last key pressed: %d", pressed);
        attroff(COLOR_PAIR(2));
        move(0, 0);
        curs_set(1);
        refresh();      /* Print it on to the real screen */

        pressed = getch();      /* Wait for user input */
        if (pressed == CTRL_Q) {
            shouldExit = true;
        }
    }
}

std::string getNewLine(DetectedNewLines newLines) {
    if (newLines.linux) {
        return "\n";
    } else if (newLines.mac) {
        return "\r";
    } else {
        return "\r\n";
    }
}

bool isPrintable(int pressed) {
    return (pressed >= 'a' and pressed <= 'z') or (pressed >= 'A' and pressed <= 'Z') or pressed == CTRL_I;
}

void printInfo(int rows, int columns, const DetectedNewLines &newLines, const std::string &filename) {
    clear();
    move(0, 0);
    printw("\nfile name: %s", filename.c_str());
    printw("\nwindow size detected: %d rows, %d columns.", rows, columns);
    std::vector<std::string> lineEndings;
    if (newLines.linux) {
        lineEndings.emplace_back("linux");
    }
    if (newLines.mac) {
        lineEndings.emplace_back("mac");
    }
    if (newLines.windows) {
        lineEndings.emplace_back("windows");
    }
    printw("\nline endings detected: %s.", concatenate(", ", lineEndings).c_str());
//    printw("\ncursor at byte: %d.", file.tellg());

    printw("\n\npress any key to exit this screen");
    refresh();      /* Print it on to the real screen */
    getch();
}

template<typename T>
std::string concatenate(std::string delimiter, T elements) {
    if (elements.size() == 0) {
        return "";
    }
    std::string result;
    for (const auto &element : elements) {
        result.append(element).append(delimiter);
    }
    for (size_t i = 0; i < delimiter.size(); ++i) {
        result.pop_back();
    }
    return result;
}

void printHelp() {
    clear();
    printw("Controls:");
    printw("\n  CTRL_b: print this help");
    printw("\n  CTRL_q: exit without saving");
    printw("\n  arrows (left, right, up, down): move within the file");
    printw("\n  CTRL_s: save edits to a separate file (can take some time)");
    printw("\n  CTRL_f: find next appearance of text");
//    printw("\n  CTRL_n: next appearance of last search");
    printw("\n  CTRL_g: go to line");
//    printw("\n  CTRL_b: go to byte number");
    printw("\n  CTRL_v: view collected statistics");
    printw("\n\npress any key to exit this help");
    refresh();
    getch();
}

void printTextInScreen(int columns, const std::vector<std::string> &textInScreen, std::pair<long, long> startHighlight,
        std::pair<long, long> endHighlight) {
    clear();
    int rowIndex = 0;
    for (const auto &row : textInScreen) {
        move(rowIndex, 0);
        if (startHighlight.second != -1 and (startHighlight.first == rowIndex or endHighlight.first == rowIndex)) {
            if ((size_t)startHighlight.second >= row.size() or (size_t)endHighlight.second >= row.size()) {
                throw std::logic_error{addStackTrace("asked to highlight columns "
                        + std::to_string(startHighlight.second) + " to " + std::to_string(endHighlight.second)
                        + " after the end of line \"" + row + "\"")};
            }
            if (startHighlight.first == endHighlight.first) {
                printw(row.substr(0, startHighlight.second).c_str());
                init_pair(1, COLOR_RED, COLOR_BLACK);
                attron(COLOR_PAIR(1));
                printw(row.substr(startHighlight.second, endHighlight.second - startHighlight.second).c_str()); // TODO out of range
                attroff(COLOR_PAIR(1));
                printw(row.substr(endHighlight.second).c_str());
            } else {
                if (startHighlight.first == rowIndex) {
                    printw(row.substr(0, startHighlight.second).c_str());
                    init_pair(1, COLOR_RED, COLOR_BLACK);
                    attron(COLOR_PAIR(1));
                    printw(row.substr(startHighlight.second).c_str());
                }
                if (endHighlight.first == rowIndex) {
                    printw(row.substr(0, endHighlight.second).c_str());
                    attroff(COLOR_PAIR(1));
                    printw(row.substr(endHighlight.second).c_str());

                }
            }
        } else {
            //        std::cout << "substr:" << subs << std::endl;
            printw(row.c_str());
        }
        rowIndex++;
    }
    refresh();
}

bool isArrow(int pressed) {
    return pressed == KEY_LEFT or pressed == KEY_RIGHT or pressed == KEY_UP or pressed == KEY_DOWN;
}

void modifyOffsets(int pressed, long &rows, long &columns, long rowsToMove, long columnsToMove) {
    switch (pressed) {
    case KEY_LEFT:
        columns = std::max(0L, columns - columnsToMove);
        break;
    case KEY_RIGHT:
        columns += columnsToMove;
        break;
    case KEY_UP:
        rows = std::max(0L, rows - rowsToMove);
        break;
    case KEY_DOWN:
        rows += rowsToMove;
        break;
    default:
        throw std::logic_error(addStackTrace(
                "Asked to move, but the key pressed was not an arrow, it had code " + std::to_string(pressed)));
    }
}
