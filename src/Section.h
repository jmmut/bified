/**
 * @file Section.h
 */

#ifndef BIGFILEEDITOR_SECTION_H
#define BIGFILEEDITOR_SECTION_H


#include <ostream>
#include <fstream>
#include <memory>
#include <vector>
#include <functional>

#if __cplusplus < 201402L
namespace std {
template<typename T, typename... Args>
std::unique_ptr<T> make_unique(Args&&... args)
{
    return std::unique_ptr<T>(new T(std::forward<Args>(args)...));
}
}
#endif

class Section;
using Sections = std::vector<std::unique_ptr<Section>>;

class Section {
public:
    virtual ~Section() = default;

    virtual void copyTo(std::ostream &out) const = 0;
    virtual void open() = 0;
    virtual int getNextLetter() = 0;
    virtual bool isOk() = 0;
    virtual void seekg(size_t positionWithinSection) = 0;
    virtual long tellg() = 0;
    virtual long getSize() const = 0;

    virtual Sections deleteLetterAt(long positionWithinSection) const = 0;
    virtual Sections insertLetterBefore(long positionWithinSection, char newLetter) const = 0;
    virtual Sections changeLetterAt(long positionWithinSection, char newLetter) const = 0;
};


std::string createStringFromChar(char newLetter) {
    return std::string(1, newLetter);
}


class ModifiedSection : public Section {

public:
    ModifiedSection(const std::string &newText) : modifiedText{newText} {}
    ~ModifiedSection() override = default;

    void copyTo(std::ostream &out) const override {
        if (not out) {
            throw std::runtime_error{"can not write section"};
        }
        out.write(modifiedText.c_str(), modifiedText.size());
    }

    long getSize() const override {
        return modifiedText.size();
    }

    Sections deleteLetterAt(long positionWithinSection) const override {
        if (positionWithinSection < 0 or positionWithinSection >= getSize()) {
            throw std::out_of_range{"asked to remove text at " + std::to_string(positionWithinSection)
                    + " which is out of this section's range [0, " + std::to_string(getSize()) + ")"};
        }
        Sections newSections;
        newSections.push_back(std::make_unique<ModifiedSection>(
                modifiedText.substr(0, positionWithinSection)
                        + modifiedText.substr(positionWithinSection + 1)));
        return newSections;
    }

    Sections insertLetterBefore(long positionWithinSection, char newLetter) const override {
        if (positionWithinSection < 0 or positionWithinSection > getSize()) {
            throw std::out_of_range{"asked to insert text before " + std::to_string(positionWithinSection)
                    + " which is out of this section's range [0, " + std::to_string(getSize()) + ")"};
        }
        Sections newSections;
        newSections.push_back(std::make_unique<ModifiedSection>(
                modifiedText.substr(0, positionWithinSection)
                        + createStringFromChar(newLetter)
                        + modifiedText.substr(positionWithinSection)));
        return newSections;
    }

    Sections changeLetterAt(long positionWithinSection, char newLetter) const override {
        if (positionWithinSection < 0 or positionWithinSection >= getSize()) {
            throw std::out_of_range{"asked to change text at " + std::to_string(positionWithinSection)
                    + " which is out of this section's range [0, " + std::to_string(getSize()) + ")"};
        }
        Sections newSections;
        newSections.push_back(std::make_unique<ModifiedSection>(
                modifiedText.substr(0, positionWithinSection)
                        + createStringFromChar(newLetter)
                        + modifiedText.substr(positionWithinSection + 1)));
        return newSections;
    }

    void open() override {
        position = 0;
    }

    int getNextLetter() override {
        return modifiedText[position++];
    }

    bool isOk() override {
        return position < modifiedText.size();
    }

    void seekg(size_t positionWithinSection) override {
        if (positionWithinSection < 0 or positionWithinSection >= modifiedText.length()) {
            throw std::out_of_range{"ModifiedSection::seekg asked to go to " + std::to_string(positionWithinSection)
            + " which is out of range [0, " + std::to_string(modifiedText.size()) + ")"};
        }
        position = positionWithinSection;
    }

    long tellg() override {
        return position;
    }

private:
    std::string modifiedText;

    size_t position;
};


/**
 * Needed to select a section to insert at the end of the file, using pattern "null object"
 */
class EndSection : public Section {

public:
    EndSection() = default;
    ~EndSection() override = default;

    void copyTo(std::ostream &out) const override {
    }

    long getSize() const override {
        return 1;
    }

    Sections deleteLetterAt(long positionWithinSection) const override {
        throw std::out_of_range{"asked to delete text after the end of the file"};
    }

    Sections insertLetterBefore(long positionWithinSection, char newLetter) const override {
        if (positionWithinSection < 0 or positionWithinSection > getSize()) {
            throw std::out_of_range{"asked to insert '" + createStringFromChar(newLetter) + "' before "
                    + std::to_string(positionWithinSection) + " which is out of this section's range [0, "
                    + std::to_string(getSize()) + ")"};
        }
        Sections newSections;
        newSections.push_back(std::make_unique<ModifiedSection>(createStringFromChar(newLetter)));
        newSections.push_back(std::make_unique<EndSection>());
        return newSections;
    }

    Sections changeLetterAt(long positionWithinSection, char newLetter) const override {
        throw std::out_of_range{"asked to change text after the end of the file"};
    }

    void open() override {
    }

    int getNextLetter() override {
        return 0;
    }

    bool isOk() override {
        return false;
    }

    void seekg(size_t positionWithinSection) override {
        throw std::logic_error{"should not need to call EndSection::seekg"};
    }

    long tellg() override {
        throw std::logic_error{"should not need to call EndSection::tellg"};
    }
};

class UnmodifiedSection : public Section {

public:
    UnmodifiedSection(std::string originalFilename, long start, long exclusiveEnd)
            : originalFilename(std::move(originalFilename)), startInOriginalFile(start),
            exclusiveEndInOriginalFile(exclusiveEnd) {
    }

    ~UnmodifiedSection() override = default;

    /**
     * TODO optimize
     */
    void copyTo(std::ostream &out) const override {
        long index = startInOriginalFile;
        std::ifstream originalFile{originalFilename};
        originalFile.clear();
        originalFile.seekg(startInOriginalFile);

        while (originalFile and index < exclusiveEndInOriginalFile) {
            index++;
            auto letter = originalFile.get();
            if (not out) {
                throw std::runtime_error{"can not write section"};
            }
            out.put(letter);
        }
        if (index != exclusiveEndInOriginalFile) {
            throw std::logic_error{"could not write the whole section of bytes [" + std::to_string(startInOriginalFile)
                    + ", " + std::to_string(exclusiveEndInOriginalFile) + ")"};
        }
    }

    long getSize() const override {
        return exclusiveEndInOriginalFile - startInOriginalFile;
    }

    Sections deleteLetterAt(long positionWithinSection) const override {
        if (positionWithinSection < 0 or positionWithinSection >= getSize()) {
            throw std::out_of_range{"asked to remove text at " + std::to_string(positionWithinSection)
                    + " which is out of this section's range [0, " + std::to_string(getSize()) + ")"};
        }
        Sections newSections;

        newSections.push_back(std::make_unique<UnmodifiedSection>(originalFilename,
                startInOriginalFile,
                startInOriginalFile + positionWithinSection));

        newSections.push_back(std::make_unique<UnmodifiedSection>(originalFilename,
                startInOriginalFile + positionWithinSection + 1,
                exclusiveEndInOriginalFile));
        return newSections;
    }

    Sections insertLetterBefore(long positionWithinSection, char newLetter) const override {
        if (positionWithinSection < 0 or positionWithinSection > getSize()) {
            throw std::out_of_range{"asked to insert text before " + std::to_string(positionWithinSection)
                    + " which is out of this section's range [0, " + std::to_string(getSize()) + ")"};
        }

        Sections newSections;

        newSections.push_back(std::make_unique<UnmodifiedSection>(originalFilename,
                startInOriginalFile,
                startInOriginalFile + positionWithinSection));

        newSections.push_back(std::make_unique<ModifiedSection>(createStringFromChar(newLetter)));

        newSections.push_back(std::make_unique<UnmodifiedSection>(originalFilename,
                startInOriginalFile + positionWithinSection,
                exclusiveEndInOriginalFile));
        return newSections;
    }

    Sections changeLetterAt(long positionWithinSection, char newLetter) const override {
        if (positionWithinSection < 0 or positionWithinSection >= getSize()) {
            throw std::out_of_range{"asked to change text at " + std::to_string(positionWithinSection)
                    + " which is out of this section's range [0, " + std::to_string(getSize()) + ")"};
        }

        Sections newSections;

        newSections.push_back(std::make_unique<UnmodifiedSection>(originalFilename,
                startInOriginalFile,
                startInOriginalFile + positionWithinSection));

        newSections.push_back(std::make_unique<ModifiedSection>(createStringFromChar(newLetter)));

        newSections.push_back(std::make_unique<UnmodifiedSection>(originalFilename,
                startInOriginalFile + positionWithinSection + 1,
                exclusiveEndInOriginalFile));

        return newSections;
    }

    void open() override {
        reader.close();
        reader.open(originalFilename);
        if (not reader) {
            throw std::runtime_error{"can not read file " + originalFilename};
        }
        reader.seekg(startInOriginalFile);
        positionWithinSection = 0;
    }

    bool isOk() override {
        return reader and positionWithinSection < getSize();
    }

    int getNextLetter() override {
        if (positionWithinSection < getSize()) {
            positionWithinSection++;
            return reader.get();
        } else {
            return 0;
        }
    }

    void seekg(size_t positionWithinSection) override {
        reader.seekg(positionWithinSection + startInOriginalFile);
        this->positionWithinSection = positionWithinSection;
    }

    long tellg() override {
        return reader.tellg() - startInOriginalFile;
    }

private:
    std::string originalFilename;
    long startInOriginalFile;
    long exclusiveEndInOriginalFile;

    std::ifstream reader;
    long positionWithinSection;
};

#endif //BIGFILEEDITOR_SECTION_H
