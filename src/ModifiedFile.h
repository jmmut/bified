/**
 * @file ModifiedFile.h
 */

#ifndef BIGFILEEDITOR_MODIFIEDFILE_H
#define BIGFILEEDITOR_MODIFIEDFILE_H

#include <map>
#include <iostream>
#include <sstream>
#include "Section.h"
#include "Reader.h"
#include "exception/StackTracedException.h"

class ModifiedFile {
public:
    ModifiedFile(const std::string &originalFilename, long cacheContext = 10000) : originalFile(originalFilename) {
        if (not originalFile) {
            throw std::runtime_error{addStackTrace("can not open file \"" + originalFilename
                    + "\" to prepare modifications to it")};
        }
        originalFile.seekg(0, std::ios::end);
        originalFileSize = originalFile.tellg();
        sections.push_back(std::make_unique<UnmodifiedSection>(originalFilename, 0, originalFileSize));
        sections.push_back(std::make_unique<EndSection>());
        sectionIndex = 0;
        sections[0]->open();
        this->cacheContext = cacheContext;
    }

    void copyTo(const std::string& filenameToSave) const {
        std::ofstream out{filenameToSave};
        if (not out) {
            // TODO check this earlier, to avoid losing changes
            throw std::runtime_error{addStackTrace("can not open file " + filenameToSave + " to save edits")};
        }
        copyTo(out);
        out.close();
    }

    void copyTo(std::ostream &out) const {
        for (const auto &section : sections) {
            section->copyTo(out);
        }
    }

    void deleteLetterAt(long positionInNewFile) {
        std::string actionMessage = "delete position " + std::to_string(positionInNewFile);
        changeSection(positionInNewFile, actionMessage, [=](const Section &section, long positionWithinSection) {
            return section.deleteLetterAt(positionWithinSection);
        });
    }

    void changeLetterAt(long positionInNewFile, char newLetter) {
        std::string actionMessage = "change position " + std::to_string(positionInNewFile) + " to '" + newLetter + "'";
        changeSection(positionInNewFile, actionMessage, [=](const Section &section, long positionWithinSection) {
            return section.changeLetterAt(positionWithinSection, newLetter);
        });
    }

    void insertLetterBefore(long positionInNewFile, char newLetter) {
        std::string actionMessage =
                "insert '" + createStringFromChar(newLetter) + "' before position " + std::to_string(positionInNewFile);
        changeSection(positionInNewFile, actionMessage, [=](const Section &section, long positionWithinSection) {
            return section.insertLetterBefore(positionWithinSection, newLetter);
        });
    }

    void changeSection(long positionInNewFile, std::string actionMessage,
            std::function<Sections(const Section &, long positionWithinSection)> action) {
        invalidateOffsetCacheAfterPosition(positionInNewFile);
        seekg(0); // invalidate reading
        long size = 0;
        long sectionStartingPosition = 0;
        for (size_t sectionIndex = 0; sectionIndex < sections.size(); ++sectionIndex) {
            auto &section = sections[sectionIndex].operator*();
            size += section.getSize();
            if (size > positionInNewFile) {
                auto newSections = action(section, positionInNewFile - sectionStartingPosition);
                auto sectionIterator = sections.begin() + sectionIndex;
                sections.erase(sectionIterator);
                sections.insert(sectionIterator,
                        std::make_move_iterator(newSections.begin()),
                        std::make_move_iterator(newSections.end()));
                return;
            }
            sectionStartingPosition = size;
        }

        throw std::out_of_range{
                addStackTrace("trying to " + actionMessage + " in a virtual file of size " + std::to_string(size))};
    }

    void invalidateOffsetCacheAfterPosition(long position) {
        auto iterator = lineToOffset.begin();
        for (; iterator != lineToOffset.end(); ++iterator) {
            if (iterator->second >= position -1) {
                break;
            }
        }
        while (iterator != lineToOffset.end()) {
            lineToOffset.erase(iterator++);
        }
    }


    // std::ifstream pseudo interface
    int get() {
        if (sectionIndex < sections.size()) {
            auto &section = sections[sectionIndex].operator*();
            if (section.isOk()) {
                return section.getNextLetter();
            } else {
                sectionIndex++;
                if (sectionIndex < sections.size()) {
                    sections[sectionIndex]->open();
                    return get();
                } else {
                    return 0;
                }
            }
        } else {
            return 0;
        }

    }

    bool isOpen() {
        return (bool)*this;
    }

    explicit operator bool() {
        if (sectionIndex < sections.size()) {
            if (sections[sectionIndex]->isOk()) {
                return true;
            } else {
                sectionIndex++;
                if (sectionIndex < sections.size()) {
                    sections[sectionIndex]->open();
                    return (bool) *this;
                } else {
                    return false;
                }
            }
        } else {
            return false;
        }
    }

    void seekg(long position) {
        long size = 0;
        long sectionStartingPosition = 0;
        for (sectionIndex = 0; sectionIndex < sections.size(); ++sectionIndex) {
            auto &section = sections[sectionIndex].operator*();
            size += section.getSize();
            if (size > position) {
                section.open();
                auto positionWithinSection = position - sectionStartingPosition;
                section.seekg(positionWithinSection);
                return;
            }
            sectionStartingPosition = size;
        }

        throw std::out_of_range{addStackTrace(
                "trying to seekg " + std::to_string(position) + " in a virtual file of size " + std::to_string(size))};
    }

    long tellg() {
        size_t sizeOfPreviousSections = 0;
        for (size_t sectionIndexIndex = 0; sectionIndexIndex < sectionIndex; ++sectionIndexIndex) {
            auto &section = sections[sectionIndexIndex].operator*();
            sizeOfPreviousSections += section.getSize();
        }
        size_t positionWithinSection;
        if (sectionIndex < sections.size()) {
            positionWithinSection = sections[sectionIndex]->tellg();
        } else {
            positionWithinSection = -EndSection{}.getSize();
        }
        return sizeOfPreviousSections + positionWithinSection;
    }

    /**
     * @param row 0-based
     * @param column 0-based
     */
    char seekCoordinateAndGet(long line, long column) {
        auto iterator = lineToOffset.find(line);
        if (iterator != lineToOffset.end()) {
            log << "line " << line << ": cache hit" << std::endl;
            auto cachedOffset = iterator->second;
            seekg(cachedOffset);
            auto letter = get();
            letter = ::seekRelativeCoordinateAndGet(*this, 0, column, letter, newLines);
            return letter;
        } else {
            auto previousIterator = lineToOffset.begin();
            auto nextIterator = lineToOffset.begin();
            for (; nextIterator != lineToOffset.end(); ++nextIterator) {
                auto cachedLine = nextIterator->first;
                if (cachedLine > line) {
                    break;
                } else {
                    previousIterator = nextIterator;
                }
            }

            if (nextIterator != lineToOffset.begin()) {
                // nextIterator is not the first, so previous iterator points to a valid cache entry.
                auto cachedLine = previousIterator->first;
                if (cachedLine > line) {
                    log << "line " << line << ": cache miss. first cached line is already past the line we want"
                            << std::endl;
                } else {
                    log << "line " << line << ": semi cache miss. hit for line " << cachedLine << std::endl;
                    auto cachedOffset = previousIterator->second;
                    seekg(cachedOffset);
                    auto letter = get();

                    auto contextLine = std::max(line - cacheContext, cachedLine);
                    auto missedLinesToContext = contextLine - cachedLine;
                    letter = ::seekRelativeCoordinateAndGet(*this, missedLinesToContext, 0, letter, newLines);
                    lineToOffset[contextLine] = tellg() - 1;
                    auto linesFromContextToRequested = line - contextLine;
                    letter = ::seekRelativeCoordinateAndGet(*this, linesFromContextToRequested, 0, letter,
                            newLines);
                    lineToOffset[line] = tellg() - 1;
                    letter = ::seekRelativeCoordinateAndGet(*this, 0, column, letter,
                            newLines);
                    return letter;
                }
            } else {
                log << "line " << line << ": cache miss. nextIterator is the first entry, "
                           "which means it already points past the line we want" << std::endl;
            }
        }
        
        // if the cache missed
        auto contextLine = std::max(line - cacheContext, 0L);
        auto letter = ::seekCoordinateAndGet(*this, contextLine, 0, newLines);
        lineToOffset[contextLine] = tellg() - 1;
        letter = ::seekRelativeCoordinateAndGet(*this, line - contextLine, 0, letter, newLines);
        lineToOffset[line] = tellg() - 1;
        letter = ::seekRelativeCoordinateAndGet(*this, 0, column, letter, newLines);
        return letter;
    }

    const std::map<long, long> &getLineToOffsetCache() const {
        return lineToOffset;
    }

    std::string getLog(){
        auto str = log.str();
        log.str("");
        return str;
    }

    const DetectedNewLines &getNewLines() const {
        return newLines;
    }

    /**
     * TODO
     * - any length for textToSearch
     * - windows newlines
     */
    std::pair<long, long> seekText(std::string textToSearch, long lineToStartSearch) {
        if (textToSearch.size() < 1) {
            throw std::logic_error(addStackTrace("text to search should be 1 char or longer"));
        }
        auto previousLetter = seekCoordinateAndGet(lineToStartSearch, 0);

        long lines = lineToStartSearch;

        while (isOpen()) {
            std::string fileLine = getLine(*this, previousLetter, newLines);
            auto foundPosition = fileLine.find(textToSearch);
            if (foundPosition != std::string::npos) {
                return {lines, foundPosition};
            }
            lines++;
        }

        return {-1, -1};
    }

private:
    std::ifstream originalFile;
    long originalFileSize;
    Sections sections;
    DetectedNewLines newLines;

    size_t sectionIndex;
    std::map<long, long> lineToOffset;
    std::stringstream log;
    long cacheContext;
};


#endif //BIGFILEEDITOR_MODIFIEDFILE_H
