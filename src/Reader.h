/**
 * @file Reader.h
 */

#ifndef BIGFILEEDITOR_READER_H
#define BIGFILEEDITOR_READER_H

#include <fstream>
#include <vector>

struct DetectedNewLines {
    bool linux = false;
    bool mac = false;
    bool windows = false;
};


bool isNewLine(char letter);

template<typename ISTREAM>
std::vector<std::string> readSquareSkipping(ISTREAM &file, long rows, long columns, long skipFirstRows,
        long skipFirstColumns, DetectedNewLines &newLines);

template<typename ISTREAM>
char consumeLettersUntilCountOrNewLine(ISTREAM &file, int howManyLettersToConsume, char letter);

template<typename ISTREAM>
char consumeLinesUntilCount(ISTREAM &file, int howManyLinesToConsume, char letter, DetectedNewLines &newLines);

template<typename ISTREAM>
char consumeUntilNewLine(ISTREAM &file, char letter);

template<typename ISTREAM>
char consumeNewLine(ISTREAM &file, char letter, DetectedNewLines &newLines);

template<typename ISTREAM>
char seekRelativeCoordinateAndGet(ISTREAM &file, long line, long column, char letter, DetectedNewLines &lines);

bool isNewLine(char letter) { return letter == '\n' or letter == '\r'; }

/**
 * Assumes the file is pointing to the first letter after a newline
 */
template<typename ISTREAM>
std::vector<std::string> readSquare(ISTREAM &file, long rows, long columns, long skipFirstColumns, int letter,
        DetectedNewLines &newLines) {
    std::vector<std::string> textInScreen;
    textInScreen.resize(rows);
    for (auto &row : textInScreen) {
        row.resize(columns);
    }

    long rowIndex = 0;
    long columnIndex = 0;
    while (file) {
        bool newLine = isNewLine(letter);
        if (not newLine) {
            textInScreen[rowIndex][columnIndex] = (char) letter;
            columnIndex++;
            letter = file.get();
        }

        if (columnIndex == columns) {
            while (file and not (newLine = isNewLine(letter))) {
                letter = file.get();
            }
            if (not file) {
                break;
            }
        }
        if (newLine) {
            letter = consumeNewLine(file, letter, newLines);
            rowIndex++;
            columnIndex = 0;
            letter = consumeLettersUntilCountOrNewLine(file, skipFirstColumns, letter);
        }
        if (rowIndex == rows) {
            break;
        }
    }
    return textInScreen;
}

template<typename ISTREAM>
char consumeLettersUntilCountOrNewLine(ISTREAM &file, int howManyLettersToConsume, char letter) {
    while (file and not isNewLine(letter) and howManyLettersToConsume > 0) {
        letter = file.get();
        howManyLettersToConsume--;
    }
    return letter;
}

template<typename ISTREAM>
char consumeLinesUntilCount(ISTREAM &file, int howManyLinesToConsume, char letter, DetectedNewLines &newLines) {
    while(file and howManyLinesToConsume > 0){
        letter = consumeUntilNewLine(file, letter);
        if (isNewLine(letter)) {
            letter = consumeNewLine(file, letter, newLines);
            howManyLinesToConsume--;
        }
    }
    return letter;
}

/**
 * Stops before consuming the newline. you have to call consumeNewLine() afterwards.
 * @return the first character of the newLine or something else if EOF (check "(bool)file")
 */
template<typename ISTREAM>
char consumeUntilNewLine(ISTREAM &file, char letter) {
    while (file and not isNewLine(letter)) {
        letter = file.get();
    }
    return letter;
}
/**
 * Stops after consuming the newline.
 * @param letter return by reference the first letter of the next line
 * @return the first character of the newLine or something else if EOF (check "(bool)file")
 */
template<typename ISTREAM>
std::string getLine(ISTREAM &file, char &letter, DetectedNewLines &newLines) {
    std::string line;
    line.push_back(letter);
    while (file and not isNewLine(letter)) {
        letter = file.get();
        line.push_back(letter);
    }

    if (isNewLine(letter)) {
        if (letter == '\n') {
            letter = file.get();
            newLines.linux = true;
        } else if (letter == '\r') {
            letter = file.get();
            if (letter == '\n') {
                line.push_back(letter);
                letter = file.get();
                newLines.windows = true;
            } else {
                newLines.mac = true;
            }
        }
    }
    return line;
}

/**
 * If the letter passed is a newline, consume it and return the next letter. If it wasn't a newline, leave the file
 * untouched and return the same letter as passed.
 *
 * @return letter after the newline, or the letter passed as parameter if it wasn't a newline
 */
template<typename ISTREAM>
char consumeNewLine(ISTREAM &file, char letter, DetectedNewLines &newLines) {
    if (letter == '\n') {
        letter = file.get();
        newLines.linux = true;
    } else if (letter == '\r') {
        letter = file.get();
        if (letter == '\n') {
            letter = file.get();
            newLines.windows = true;
        } else {
            newLines.mac = true;
        }
    }
    return letter;
}

template<typename ISTREAM>
char seekCoordinateAndGet(ISTREAM &file, long line, long column, DetectedNewLines &lines) {
    file.seekg(0);
    auto letter = file.get();
    letter = seekRelativeCoordinateAndGet(file, line, column, letter, lines);
    return letter;
}

template<typename ISTREAM>
char seekRelativeCoordinateAndGet(ISTREAM &file, long line, long column, char letter, DetectedNewLines &lines) {
    letter = consumeLinesUntilCount(file, line, letter, lines);
    letter = consumeLettersUntilCountOrNewLine(file, column, letter);
    return letter;
}


template<typename ISTREAM>
std::vector<std::string> readSquareSkipping(ISTREAM &file, long rows, long columns, long skipFirstRows,
        long skipFirstColumns,
        DetectedNewLines &newLines) {
    auto letter = seekCoordinateAndGet(file, skipFirstRows, skipFirstColumns, newLines);
    return readSquare(file, rows, columns, skipFirstColumns, letter, newLines);
}

#endif //BIGFILEEDITOR_READER_H
