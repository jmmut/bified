/**
 * @file StackTracedException.cpp
 */

#include <vector>
#include <tuple>
#include "StackTracedException.h"

StackTracedException::StackTracedException(const std::string &message) {
    std::stringstream ss;
    ss << message << std::endl;
    print_stacktrace(ss);
    this->message = ss.str();
}

const char *StackTracedException::what() const noexcept {
    return message.c_str();
}



#if !defined DO_NOT_BACKTRACE && !defined WIN32

std::pair<std::vector<std::string>, int> runSystemCall(const std::string &command);

std::string getFileAndLine(const std::string &programName, const std::string &functionName,
        const std::string &offsetString);

void *parsePointer(const std::string &pointerString);

/** Print a demangled stack backtrace of the caller function to ostream out.
 * Courtesy of (c) 2008, Timo Bingmann from http://idlebox.net/
 * published under the WTFPL v2.0
 */
void print_stacktrace(std::ostream &out, unsigned int max_frames) {
    auto stack = print_stacktrace(max_frames);
    auto printStackFunctionsToSkip = 2;
    for (size_t i = printStackFunctionsToSkip; i < stack.size(); ++i) {
        out << "    " << stack[i] << "\n";
    }
}
std::vector<std::string> print_stacktrace(unsigned int max_frames) {
    std::vector<std::string> stack;

    // storage array for stack trace address data
    void* addrlist[max_frames+1];

    // retrieve current stack addresses
    int addrlen = backtrace(addrlist, sizeof(addrlist) / sizeof(void*));

    if (addrlen == 0) {
        stack.emplace_back("<empty, possibly corrupt>");
        return stack;
    }

    // resolve addresses into strings containing "filename(function+address)",
    // this array must be free()-ed
    char** symbollist = backtrace_symbols(addrlist, addrlen);


    // allocate string which will be filled with the demangled function name
    size_t funcnamesize = 256;
    char* demangledFunctionName = (char*)malloc(funcnamesize);


    // iterate over the returned symbol lines. skip the first, it is the
    // address of this function.
    for (int i = 1; i < addrlen; i++)
    {
        char *begin_name = 0, *begin_offset = 0, *end_offset = 0;

        // find parentheses and +address offset surrounding the mangled name:
        // ./module(function+0x15c) [0x8048a6d]
        for (char *p = symbollist[i]; *p; ++p)
        {
            if (*p == '(')
                begin_name = p;
            else if (*p == '+')
                begin_offset = p;
            else if (*p == ')' && begin_offset) {
                end_offset = p;
                break;
            }
        }

        std::string programName{symbollist[i], begin_name};
        std::string functionName{begin_name+1, begin_offset};
        std::string offsetString{begin_offset+1, end_offset};
        std::string fileAndLine = getFileAndLine(programName, functionName, offsetString);

        std::string location = programName;
        if (not fileAndLine.empty()) {
            location.append(":").append(fileAndLine);
        }

        if (begin_name && begin_offset && end_offset
            && begin_name < begin_offset)
        {
            *begin_name++ = '\0';
            *begin_offset++ = '\0';
            *end_offset = '\0';

            // mangled name is now in [begin_name, begin_offset) and caller
            // offset in [begin_offset, end_offset). now apply
            // __cxa_demangle():

            int status;
            char* ret = abi::__cxa_demangle(functionName.c_str(), demangledFunctionName, &funcnamesize, &status);
            if (status == 0) {
                demangledFunctionName = ret; // use possibly realloc()-ed string
                std::string line;
                line.append(location).append(" : ").append(demangledFunctionName).append("+").append(offsetString);
                stack.push_back(line);
            } else {
                // demangling failed. Output function name as a C function with
                // no arguments.
                std::string line;
                line.append(location).append(" : ").append(functionName).append("()+").append(offsetString);
                stack.push_back(line);
            }
        } else {
            // couldn't parse the line? print the whole line.
            stack.emplace_back(symbollist[i]);
        }
    }

    free(demangledFunctionName);
    free(symbollist);
    return stack;
}

std::string getFileAndLine(const std::string &programName, const std::string &functionName,
        const std::string &offsetString) {
    std::string fileAndLine;

    auto command = "nm " + programName + " 2>/dev/null | grep -w " + functionName;
    std::vector<std::string> nmOutput;
    int nmReturnStatus;
    std::tie(nmOutput, nmReturnStatus) = runSystemCall(command);

    if (nmReturnStatus == 0) {
        void *funcAddress = parsePointer(nmOutput[0]);
        void *offset = parsePointer(offsetString);
        void *absoluteAddress = (void *) ((long) funcAddress + (long) offset);

        std::stringstream command2ss;
        command2ss << "addr2line -e " << programName << " " << absoluteAddress;
        std::vector<std::string> a2lOutput;
        int a2lReturnStatus;
        std::tie(a2lOutput, a2lReturnStatus) = runSystemCall(command2ss.str());

        if (a2lReturnStatus == 0) {
            auto lastDirectory = a2lOutput[0].rfind('/');
            if (lastDirectory != std::string::npos) {
                fileAndLine = a2lOutput[0].substr(lastDirectory + 1);
                fileAndLine.pop_back(); // remove end of line
                auto restOfTheLine = fileAndLine.find(' ');
                if (restOfTheLine != std::string::npos) {
                    fileAndLine = fileAndLine.substr(0, restOfTheLine);
                }
            }
        }
    }
    return fileAndLine;
}

void *parsePointer(const std::string &pointerString) {
    std::stringstream ss;
    ss << std::hex << pointerString;
    void *funcAddress;
    ss >> funcAddress;
    return funcAddress;
}

std::pair<std::vector<std::string>, int> runSystemCall(const std::string &command) {
    FILE *fp = popen(command.c_str(), "r");
    char buf[1024];
    std::vector<std::string> resultLines;
    
    while (fgets(buf, 1024, fp)) {
        resultLines.emplace_back(buf);
    }

    return {resultLines, WEXITSTATUS(fclose(fp))};
}

#else // DO_NOT_BACKTRACE

void print_stacktrace(std::ostream &out, unsigned int max_frames) {}

#endif // DO_NOT_BACKTRACE
