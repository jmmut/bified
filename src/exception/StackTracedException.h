/**
 * @file StackTracedException.h
 */

#ifndef COMMONLIBS_STACKTRACEDEXCEPTION_H
#define COMMONLIBS_STACKTRACEDEXCEPTION_H


// in case the user doesn't have execinfo.h, or unix's signals: disable backtracing on segfaults:
//#define DO_NOT_BACKTRACE


#if !defined DO_NOT_BACKTRACE && !defined WIN32

#include <signal.h>
#include <errno.h>
#include <execinfo.h>
#include <sys/types.h>
#include <unistd.h>
#include <cxxabi.h>
#endif // DO_NOT_BACKTRACE

#include <iostream>
#include <sstream>
#include <stdexcept>

/**
 * exception that carries the call stack in the message. Use as a regular exception:

    if (badcondition) {
        throw StackTracedException("something went wrong");
    }
 */
class StackTracedException : public std::exception {
private:
    std::string message;

public:
    StackTracedException(const std::string &message);
    virtual const char *what() const noexcept override;
};

void print_stacktrace(std::ostream &out, unsigned int max_frames = 63);

std::vector<std::string> print_stacktrace(unsigned int max_frames = 63);


/**
 * Throws an exception of type EXCEPTION with the stack trace of where it was thrown.
 *
 * @tparam EXCEPTION has to have a constructor that receives a std::string
 */
template<typename EXCEPTION>
EXCEPTION withStackTrace(std::string exceptionMessage) {
    std::stringstream messageBuilder;
    messageBuilder << "Exception: " << exceptionMessage;
    messageBuilder << "\nThrown at:\n";
    print_stacktrace(messageBuilder);
    return EXCEPTION{messageBuilder.str()};
}

inline std::string addStackTrace(std::string exceptionMessage) {
    std::stringstream messageBuilder;
    messageBuilder << "Exception: " << exceptionMessage;
    messageBuilder << "\nThrown at:\n";
    print_stacktrace(messageBuilder);
    return messageBuilder.str();
}

#endif //COMMONLIBS_STACKTRACEDEXCEPTION_H
