# Big File Editor (bified)

License: [Apache 2.0](http://www.apache.org/licenses/LICENSE-2.0)

This is a simplified plain text editor, designed to work with big files.

The usual way to edit big files (in linux) is often a manual combination of `head`, `tail`, `grep`, `sed` and `cat`.

This program does basically the same, through an interface similar to a text editor in a terminal (like vim, nano or emacs).

Features:

```
[ ] Performance
  [ ] Production ready stability (CURRENTLY IT'S HIGHLY EXPERIMENTAL! EXPECT BUGS!)
  [✓] Cache line offsets to minimise file reading
  [✓] Minimal memory usage, regardless of size of input file (4 MB editing a file of 76 MB, compared to 140 MB that vim needs)
  [ ] Decent loading speeds (bified jumping to a line is 10 times slower than vim loading the entire file in memory)
  [ ] Consistent key commands locally and over ssh
[ ] Browsing file
  [✓] Draw text in the terminal using ncurses library
  [✓] Scroll horizontally and vertically
  [✓] Jump to a given line (works but it's slow)
  [ ] Jump to a given byte
  [✓] Find text
  [ ] Jump to next occurrence of last text search
[ ] Editing file
  [✓] Delete the character under the cursor (using the "delete" key)
  [✓] Delete the character before the cursor (using the "backspace" key)
  [✓] Insert letters and numbers before the cursor
  [✓] Insert newlines
  [ ] Copy/paste whole lines
  [ ] Copy/paste selection?
  [✓] Save the edited file
  [ ] Tell where the saved copy is
```

## Requirements

- C++ compiler (`sudo apt-get install build-essential`)
- cmake (`sudo apt-get install cmake`)
- conan (download [here](https://www.conan.io/downloads), recommended:
  `sudo apt-get install python-pip; sudo pip install setuptools wheel conan`)

### Optional

- ninja to substitute makefile (`sudo apt-get install ninja-build`)

### Alternative

If you don't want to install conan nor cmake, you can install ncurses in your system and link to it
```
sudo apt-get install libncurses5
```

## Build

```
mkdir build && cd build
conan install .. --build missing
cmake -G Ninja .. # or -G "Unix Makefiles"
ninja  # or make
```

### Alternative

```
g++ -c src/exception/StackTracedException.cpp -o StackTracedException.o --std=c++11 -DDO_NOT_BACKTRACE
g++ src/main.cpp StackTracedException.o -o bified -lncurses --std=c++11
```

## Run

```
./bin/bified your_file.txt
``` 

After running the program, press CTRL_b to see the list of available actions. 
